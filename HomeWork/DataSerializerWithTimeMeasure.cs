﻿using System;
using System.Diagnostics;

namespace HomeWork
{
	class DataSerializerWithTimeMeasure
	{
		IDataSerializer _dataSerializer;
		public int Count { get => _count; set => _count = value > 0 ? value : 1; }
		int _count;
		public DataSerializerWithTimeMeasure(IDataSerializer dataSerializer, int count = 1)
		{
			_dataSerializer = dataSerializer ?? throw new ArgumentNullException(nameof(dataSerializer));
			Count = count;
		}
		public float MeasureTimeToDeserrializeData(string data)
		{
			var sw = new Stopwatch();
			sw.Start();
			for (int i = 0; i < Count; i++)
				_dataSerializer.DeserrializeData(data);
			sw.Stop();
			return (float)sw.ElapsedMilliseconds/1000;
		}

		public float MeasureTimeToSerrializeData(Data data)
		{
			var sw = new Stopwatch();
			sw.Start();
			for (int i = 0; i < Count; i++)
				_dataSerializer.SerrializeData(data);
			sw.Stop();
			return (float)sw.ElapsedMilliseconds / 1000;
		}
	}
}
