﻿using System;

namespace HomeWork
{
	class Program
	{
		static void Main(string[] args)
		{
			var count = 5_000_000;
			var data = Data.Get();
			var jsonSer = new DataSerializerJson();
			var csvSer = new DataSerializerCsv();
			var csvSerWithoutRefl = new DataSerializerCsvWithoutReflection();
			var dataJson = jsonSer.SerrializeData(data);
			var dataCsv = csvSer.SerrializeData(data);
			var dataCsvWithoutRefl = csvSerWithoutRefl.SerrializeData(data);
			var jsonTimeMeasure = new DataSerializerWithTimeMeasure(jsonSer, count);
			var csvTimeMeasure = new DataSerializerWithTimeMeasure(csvSer, count);
			var csvTimeMeasureWithoutRefl = new DataSerializerWithTimeMeasure(csvSerWithoutRefl, count);
			Console.WriteLine($"На {count:N} серриализаций в JSON потребовалось {jsonTimeMeasure.MeasureTimeToSerrializeData(data)} секунд");
			Console.WriteLine($"На {count:N} серриализаций в CSV с рефлексией потребовалось {csvTimeMeasure.MeasureTimeToSerrializeData(data)} секунд");
			Console.WriteLine($"На {count:N} серриализаций в CSV без рефлексии потребовалось {csvTimeMeasureWithoutRefl.MeasureTimeToSerrializeData(data)} секунд");
			Console.WriteLine();
			Console.WriteLine($"На {count:N} десерриализаций из JSON потребовалось {jsonTimeMeasure.MeasureTimeToDeserrializeData(dataJson)} секунд");
			Console.WriteLine($"На {count:N} десерриализаций из CSV с рефлексией потребовалось {csvTimeMeasure.MeasureTimeToDeserrializeData(dataCsv)} секунд");
			Console.WriteLine($"На {count:N} десерриализаций из CSV без рефлексии потребовалось {csvTimeMeasureWithoutRefl.MeasureTimeToDeserrializeData(dataCsvWithoutRefl)} секунд");
		}
	}
}
