﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HomeWork
{
	public class DataSerializerCsv : IDataSerializer
	{
		public Data DeserrializeData(string data)
		{
			var type = typeof(Data);
			var result = Activator.CreateInstance(type);
			var fields = type.GetFields();
			var props = type.GetProperties();
			var rows = data.Split("\n");
			foreach(var row in rows)
			{
				var cols = row.Split(",");
				var field = fields.FirstOrDefault(f => f.Name == cols[0]);
				if (field != null)
				{
					var stringValue = cols[1];
					var fieldType = field.FieldType;
					if (fieldType == stringValue.GetType())
						field.SetValue(result, stringValue);
					else
					{
						//Тут походу нужен switch кейс из всех стандартных типов, но это очень долго.
						//т.к. мы знаем что все поля в Date int то их и будем получать из строк
						var fieldTypeValue = Convert.ToInt32(stringValue);
						field.SetValue(result, fieldTypeValue);
					}
				}
				else
				{
					var prp = props.FirstOrDefault(f => f.Name == cols[0]);
					if (prp != null)
						prp.SetValue(result, cols[1]);
					//Тут аналогично мы знаем что нет полей в классе, поэтому оставим недореализованным
				}
			}

			return (Data)result;
		}

		public string SerrializeData(Data data)
		{
			var result = new StringBuilder();
			var type = data.GetType();

			var fields = type.GetFields();
			foreach(var field in fields)
			{
				var row = $"{field.Name},{field.GetValue(data)}\n";
				result.Append(row);
			}

			var props = type.GetProperties();
			foreach (var prp in props)
			{
				var row = $"{prp.Name},{prp.GetValue(data)}\n";
				result.Append(row);
			}

			return result.ToString();
		}
	}
}
