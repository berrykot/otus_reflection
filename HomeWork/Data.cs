﻿using System;

namespace HomeWork
{
	[Serializable]
	public class Data
	{
		public int i1, i2, i3, i4, i5;
		public static Data Get() => new Data() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
	}
}
