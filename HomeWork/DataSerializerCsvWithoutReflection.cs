﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork
{
	class DataSerializerCsvWithoutReflection : IDataSerializer
	{
		//В одной строке в первом столбце - имя переменной, во втором столбце - значение. Столбцы разделяем запятой.
		const string template = "i1,{0}\ni2,{1}\ni3,{2}\ni4,{3}\ni5,{4}\n";
		public Data DeserrializeData(string data)
		{
			var result = new Data();
			var rows = data.Split("\n");
			for (int i = 0; i < rows.Length; i++)
			{
				var cols = rows[i].Split(",");
				if(i == 0)
					result.i1 = int.Parse(cols[1]);
				else if (i == 1)
					result.i2 = int.Parse(cols[1]);
				else if (i == 2)
					result.i3 = int.Parse(cols[1]);
				else if (i == 3)
					result.i4 = int.Parse(cols[1]);
				else if (i == 4)
					result.i5 = int.Parse(cols[1]);
			}
			return result;
		}

		public string SerrializeData(Data data)
		{
			return string.Format(template, data.i1, data.i2, data.i3, data.i4, data.i5);
		}
	}
}
