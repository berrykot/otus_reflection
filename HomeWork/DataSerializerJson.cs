﻿using System;
using Newtonsoft.Json;

namespace HomeWork
{
	public class DataSerializerJson : IDataSerializer
	{
		public Data DeserrializeData(string data)
		{
			return JsonConvert.DeserializeObject<Data>(data);
		}

		public string SerrializeData(Data data)
		{
			return JsonConvert.SerializeObject(data);
		}
	}
}
