﻿using System;
using System.Reflection;
using System.Linq;


namespace reflection
{
	class ReflectionPractice
	{
		public void Practice()
		{
			WorkWithPrivateField();
			WorkWithAssambly();
			WorkWithInstance();
		}

		public void WorkWithPrivateField()
		{
			var inst = new ReflectionTester();
			var testType = GetTestType(1);
			var x = testType.GetProperty("obj", BindingFlags.NonPublic | BindingFlags.Instance);
			x.SetValue(inst, new { Name = "V", Age = 10 });
			var y = x.GetValue(inst);
		}

		public void WorkWithAssambly()
		{
			var ass = GetAssambly(2);
			var x = ass.GetTypes();
		}

		public void WorkWithInstance()
		{
			var instance = CreateInstance(3);
		}

		private Type GetTestType(int way)
		{
			if (way == 1)
				return typeof(ReflectionTester);

			else if (way == 2)
				return new ReflectionTester().GetType();

			else
				return Type.GetType("reflection.ReflectionTester");
		}


		private TestAttribute GetTestAttribute()
		{
			return typeof(ReflectionTester).GetCustomAttributes(false).FirstOrDefault(a => a is TestAttribute) as TestAttribute;
		}


		private Assembly GetAssambly(int way)
		{
			if (way == 1)
				return Assembly.Load("reflection");

			else if (way == 2)
				return Assembly.LoadFrom("reflection.exe");

			else
				return Assembly.LoadFile(@"C:\Users\Kot\source\repos\reflection\reflection\bin\Debug\reflection.exe");
		}

		private object CreateInstance(int way)
		{
			if (way == 1)
				return Activator.CreateInstance(typeof(ReflectionTester));

			else if (way == 2)
				return Activator.CreateComInstanceFrom("reflection.exe", "reflection.ReflectionTester").Unwrap();

			else
				return Activator.CreateInstance<ReflectionTester>();
		}

	}
}
