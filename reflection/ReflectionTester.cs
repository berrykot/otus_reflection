﻿using System;

namespace reflection
{
	
	[TestBase]
	public class ReflectTesterBase
	{
		public string BaseProp { get; set; }
		public static int BaseInt;
	}

	[Test(8)]
	class ReflectionTester : ReflectTesterBase
	{
		public ReflectionTester() { }
		ReflectionTester(int i) { pi = i; }
		~ReflectionTester() { }
		const string pcs = "private const string";
		int pi;
		event EventHandler evnt;
		object obj { get; set; }
		void DoSmth() { }
		public static ReflectionTester operator +(ReflectionTester c1, ReflectionTester c2) => new ReflectionTester(c1.pi + c2.pi);
		int this[int index]
		{
			get
			{
				return pi;
			}
			set
			{
				pi = value;
			}
		}
	}
}
