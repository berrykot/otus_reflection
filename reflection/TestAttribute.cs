﻿using System;

namespace reflection
{
	class TestAttribute : Attribute
	{
		public TestAttribute(int i) => MyProperty = i; 
		public int MyProperty { get; set; }
	}
	class TestBaseAttribute : Attribute
	{
	}
}
